package com.example.demo.entity;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity 
public class AccountEntity {
	@Id
	public String email;
	public String password;
	public String username;
	public int phone;
	
	
	public AccountEntity() {}
	
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public int getPhone() {
		return phone;
	}
	public void setPhone(int phone) {
		this.phone = phone;
	}


}
