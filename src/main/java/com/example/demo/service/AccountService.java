package com.example.demo.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demo.entity.AccountEntity;
import com.example.demo.model.Account;
import com.example.demo.repository.AccountRep;

@Service
public class AccountService {

	@Autowired
	AccountRep ar;
	
	public List<Account> getaccounts(){
		try {
		List<AccountEntity> entitylist= ar.findAll();
		List<Account> accountlist= new ArrayList<Account>();
		entitylist.stream().forEach (e -> { 
			Account acc = new Account();
			BeanUtils.copyProperties(e, acc);
			accountlist.add(acc);
		});
		return accountlist;
		}catch(Exception e) {throw e;}
	}
	public boolean createaccount(AccountEntity ae) {
		try {
			if (ar.findById(ae.getEmail()).isEmpty()) {
				ar.save(ae);
				return true;
			}
			else {
				return false;
			}
		}catch(Exception e) {throw e;}
	}
	public Account login(String email,String pass) {
		try {
			if (ar.findById(email).isEmpty()) {
				return null;
			}
			else {
				AccountEntity ae= ar.findById(email).get();
				if (pass==ae.getPassword()) {
					Account a= new Account();
					BeanUtils.copyProperties(ae, a);
					return a;
				}
				else {
					Account a = new Account();
					return a;
				}
			}
		}catch (Exception e) {
			// TODO: handle exception
			throw e;
		}
	}
}

